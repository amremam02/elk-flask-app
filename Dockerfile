FROM tiangolo/uwsgi-nginx-flask:python3.7
MAINTAINER Amr Emam amr.emam07@gmail.com

###############################################################################
#               Configure Nginx
###############################################################################


# Setup other tools
RUN apt-get update \
 && apt-get install -y curl \
 && apt-get clean

# remove log symlinks
RUN unlink /var/log/nginx/error.log
RUN unlink /var/log/nginx/access.log

# Setup nginx caching requrements
RUN mkdir -p /tmp/nginx/cache && \
	chown nginx:nginx /tmp/nginx/cache

# COPY nginx.conf 
ADD ./templates/nginx.conf /etc/nginx/nginx.conf

# COPY Flask App
COPY ./app /app

# install flask hello world app requirements
RUN pip install -r /app/requirements.txt

###############################################################################
#		Install Filebeat
###############################################################################

ENV FILEBEAT_VERSION 6.5.1

# Install Filebeat
RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-${FILEBEAT_VERSION}-amd64.deb \
 && dpkg -i filebeat-${FILEBEAT_VERSION}-amd64.deb \
 && rm filebeat-${FILEBEAT_VERSION}-amd64.deb

# configure Filebeat

# config file
ADD ./filebeat/filebeat.yml /etc/filebeat/filebeat.yml
RUN chmod 644 /etc/filebeat/filebeat.yml

ADD ./templates/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
# RUN chmod +x /usr/bin/supervisord

# CA cert
RUN mkdir -p /etc/pki/tls/certs
ADD ./filebeat/logstash-beats.crt /etc/pki/tls/certs/logstash-beats.crt

# create template based on filebeat version (assumption: it is the same version as elasticsearch version)
RUN filebeat export template --es.version ${FILEBEAT_VERSION} > /etc/filebeat/filebeat.template.json

###############################################################################
#               Start Filebeat
###############################################################################

ADD ./filebeat/start.sh /usr/local/bin/start.sh
RUN chmod +x /usr/local/bin/start.sh
#CMD /usr/local/bin/start.sh

