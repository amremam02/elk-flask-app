This is a demo program for a sample 'Hello World' flask application that uses uwsgi and Nginx
The Nginx logs are stored in elasticsearch and ELK stack 

Prerequisites:
	Docker installed on host system and a minumum of 4GB RAM host system

Installation:

	# clone the git repo
	git clone https://gitlab.com/amremam02/elk-flask-app.git

	# build the flask applicaiton machine
	docker build -t uwsgi-nginx-flask-hello .

	# docker compose run
	nohup docker-compose up &

Uninstallation:

	# stop containers and delete images 
	docker stop $(docker ps -q -a) && docker rm $(docker ps -q -a)
